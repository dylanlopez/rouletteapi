﻿using API.Models;
using Microsoft.AspNetCore.Mvc;
using ServiceStack.Redis;
using System;
using System.Collections.Generic;

namespace API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class BetController : ControllerBase
    {
        [HttpPost("create")]
        public dynamic BetInRoulette([FromBody] Bet request)
        {
            var user = Request.Headers["User"];
            using (var redis = new RedisClient())
            {
                var betsRoulettes = redis.As<Bet>();
                request.User = user;
                betsRoulettes.Store(request);

                return new
                {
                    code = 200,
                    message = "Ok",
                    data = new { state = string.Format("Bet of ${0} to roulette {1} is Created", request.Amount, request.IdRoulette) }
                };
            }
        }

        internal CloseRouletteResponse GetListBetsById(IList<Bet> bets, long id)
        {
            var response = new CloseRouletteResponse();
            var listBets = new List<Bet>();
            var totalAmount = Convert.ToDecimal(0);
            var totalBets = 0;
            foreach (var bet in bets)
            {
                if (bet.IdRoulette == id)
                {
                    totalAmount = totalAmount + bet.Amount;
                    totalBets++;
                    listBets.Add(bet);
                }
            }
            response.TotalAmount = totalAmount;
            response.TotalBets = totalBets;
            response.ListBets = listBets;
            
            return response;
        }
    }
}
