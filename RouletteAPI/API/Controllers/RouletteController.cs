﻿using API.Models;
using Microsoft.AspNetCore.Mvc;
using ServiceStack.Redis;

namespace API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class RouletteController : ControllerBase
    {
        private BetController _betController;
        private long _id;

        public RouletteController()
        {
            _id = GetMaxIdRoulette();
        }

        private long GetMaxIdRoulette()
        {
            long max = -1;
            using (var redis = new RedisClient())
            {
                var redisRoulettes = redis.As<Roulette>();
                var roulettes = redisRoulettes.GetAll();
                foreach (var roulette in roulettes)
                {
                    if (roulette.Id > max)
                    {
                        max = roulette.Id;
                    }
                }
                return max;
            }
        }

        [HttpGet("create")]
        public dynamic CreateRoulette()
        {
            _id++;
            using (var redis = new RedisClient())
            {
                var redisRoulettes = redis.As<Roulette>();
                var newRoulette = new Roulette { Id = _id, IsOpen = false };
                redisRoulettes.Store(newRoulette);
                var createdId = _id;

                return new { code = 200, 
                    message = "Ok", 
                    data = new { id = createdId } };
            }
        }

        [HttpPost("open")]
        public dynamic OpenRoulette([FromBody] Roulette request)
        {
            using (var redis = new RedisClient())
            {
                var redisRoulettes = redis.As<Roulette>();
                var roulette = redisRoulettes.GetById(request.Id);
                roulette.IsOpen = true;
                redisRoulettes.Store(roulette);

                return new { code = 200, 
                    message = "Ok", 
                    data = new { state = string.Format("Roulette {0} is Open", roulette.Id) } };
            }
        }

        [HttpPost("close")]
        public dynamic CloseRoulette([FromBody] Roulette request)
        {
            using (var redis = new RedisClient())
            {
                var redisRoulettes = redis.As<Roulette>();
                var roulette = redisRoulettes.GetById(request.Id);
                roulette.IsOpen = false;
                redisRoulettes.Store(roulette);
                var betsRoulettes = redis.As<Bet>();
                var bets = betsRoulettes.GetAll();
                _betController = new BetController();
                var response = _betController.GetListBetsById(bets, request.Id);
                response.GenerateRandomNumberWinner();
                response.SearchWinnerByNumber();
                if (response.PricePool == 0)
                {
                    response.SearchWinnerByColor();
                }

                return new { code = 200, 
                    message = "Ok", 
                    data = new { pricePool = response.PricePool, randomNumberWinner = response.RandomNumberWinner, totalAmount = response.TotalAmount, totalBets = response.TotalBets, winner = response.Winner } };
            }
        }

        [HttpGet("get")]
        public dynamic GetRoulettes()
        {
            using (var redis = new RedisClient())
            {
                var redisRoulettes = redis.As<Roulette>();
                var roulettes = redisRoulettes.GetAll();

                return new
                {
                    code = 200,
                    message = "Ok",
                    data = roulettes
                };
            }
        }

        [HttpDelete("delete")]
        public dynamic DeleteRoulettes()
        {
            using (var redis = new RedisClient())
            {
                var redisRoulettes = redis.As<Roulette>();
                redisRoulettes.DeleteAll();
                var betsRoulettes = redis.As<Bet>();
                betsRoulettes.DeleteAll();

                return new
                {
                    code = 200,
                    message = "Ok",
                    data = new { status = "Success" }
                };
            }
        }
    }
}
