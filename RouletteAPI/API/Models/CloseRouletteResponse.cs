﻿using System;
using System.Collections.Generic;

namespace API.Models
{
    public class CloseRouletteResponse
    {
        public CloseRouletteResponse()
        {
            PricePool = Convert.ToDecimal(0);
            TotalAmount = Convert.ToDecimal(0);
            TotalBets = 0;
            Winner = string.Empty;
        }

        public List<Bet> ListBets { get; set; }
        public decimal PricePool { get; set; }
        public int RandomNumberWinner { get; set; }
        public decimal TotalAmount { get; set; }
        public int TotalBets { get; set; }
        public string Winner { get; set; }

        internal void GenerateRandomNumberWinner()
        {
            Random rnd = new Random();
            RandomNumberWinner = rnd.Next(0, 37);
        }
        internal void SearchWinnerByColor()
        {
            foreach (var itemBet in ListBets)
            {
                if (itemBet.Color.Equals("N"))
                {
                    if (RandomNumberWinner % 2 == 0)
                    {
                        PricePool = itemBet.Amount * Convert.ToDecimal(1.8);
                        Winner = itemBet.User;
                    }
                }
                else if (itemBet.Color.Equals("R"))
                {
                    if (RandomNumberWinner % 2 == 1)
                    {
                        PricePool = itemBet.Amount * Convert.ToDecimal(1.8);
                        Winner = itemBet.User;
                    }
                }
            }
        }
        internal void SearchWinnerByNumber()
        {
            foreach (var itemBet in ListBets)
            {
                if (itemBet.Number == RandomNumberWinner)
                {
                    PricePool = itemBet.Amount * 5;
                    Winner = itemBet.User;
                }
            }
        }
    }
}
