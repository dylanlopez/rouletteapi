﻿namespace API.Models
{
    public class Bet
    {
        public int Number { get; set; }
        public string Color { get; set; }
        public decimal Amount { get; set; }
        public string User { get; set; }
        public long IdRoulette { get; set; }
    }
}
